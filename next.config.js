/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  experimental: { appDir: true },
  env: {
    SERVER: process.env.SERVER,
    IMAGE: process.env.IMAGE,
    // envi: "akármi",
  },
  images: {
    domains: ["dsjdsj", process.env.IMAGE || ""],
  },
};

module.exports = nextConfig;
