"use client";
import { useEffect, useRef, useState } from "react";
import gsap from "gsap";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";

type Props = {
  src: string;
  steps: number[];
  pinnedElementId: string;
  scrollDistanceModifier: number;
  onStepStarted?: Function;
  once?: boolean;
  width?: string | number;
  height?: string | number;
  children?: React.ReactNode;
};

export default function SnapVideo({
  src,
  steps,
  pinnedElementId,
  scrollDistanceModifier,
  onStepStarted,
  once,
  width,
  height,
  children,
}: Props) {
  gsap.registerPlugin(ScrollTrigger);

  const videoRef = useRef<HTMLVideoElement>(null);
  const [duration, setDuration] = useState(0);
  const [currentStep, setCurrentStep] = useState(0);
  const [scrollProgress, setScrollProgress] = useState(0);
  const [videoProgress, setVideoProgress] = useState(0);
  const [videoSizePostfix, setVideoSizePostfix] = useState<null | "" | "_2x">(
    null
  );

  useEffect(() => {
    if (videoRef.current && duration && scrollProgress) {
      if (
        scrollProgress <= steps[currentStep] / duration &&
        videoRef.current.currentTime >= steps[currentStep] &&
        !videoRef.current.paused &&
        !videoRef.current.ended
      ) {
        videoRef.current.pause();
      }

      if (
        scrollProgress > steps[currentStep] / duration &&
        videoRef.current.currentTime >= steps[currentStep] &&
        !videoRef.current.paused &&
        !videoRef.current.ended
      ) {
        setCurrentStep(currentStep + 1);
        onStepStarted ? onStepStarted(currentStep + 1) : null;
      }

      if (
        scrollProgress > steps[currentStep] / duration &&
        videoRef.current.paused &&
        !videoRef.current.ended
      ) {
        setCurrentStep(currentStep + 1);
        onStepStarted ? onStepStarted(currentStep + 1) : null;
        videoRef.current.play();
      }
    }
  }, [
    scrollProgress,
    duration,
    currentStep,
    steps,
    videoProgress,
    onStepStarted,
  ]);

  useEffect(() => {
    if (steps.length < 2) {
      return;
    }

    const matches =
      window.matchMedia("(max-resolution: 1x)").matches ||
      window.matchMedia("(-webkit-max-device-pixel-ratio: 1");
    setVideoSizePostfix(matches ? "_2x" : "");

    let ctx = gsap.context(() => {
      if (videoRef.current) {
        videoRef.current.load();

        videoRef.current.addEventListener("loadedmetadata", () => {
          if (videoRef.current?.duration) {
            setDuration(videoRef.current.duration);
            videoRef.current.pause();
          }
        });

        ScrollTrigger.create({
          trigger: `#${pinnedElementId}`,
          pin: true,
          pinSpacing: true,
          start: "top top",
          end: () => `+=${window.innerHeight * scrollDistanceModifier}`,
          markers: true,
          once: once,
          onUpdate: (self) => setScrollProgress(self.progress),
          onEnter: () => {
            videoRef.current?.play();
            setCurrentStep(0);
            onStepStarted ? onStepStarted(currentStep) : null;
          },
          onLeave: (self) => {
            if (once) {
              self.scroll(self.start);
              self.disable();
            }
          },
          onLeaveBack: () => {
            if (!once) {
              videoRef.current?.load();
              videoRef.current?.pause();
            }
          },
        });
      }
    });

    return () => ctx.revert();
  }, []);

  return (
    <>
      <section style={{ background: "white" }}>
        <h2>{duration ? `Video duration: ${duration}` : "video loading..."}</h2>
        <h2>{`Scroll progress: ${scrollProgress}`}</h2>
        <h2>{`steps: ${steps}`}</h2>
        <h2>{`to ${steps[currentStep]} sec`}</h2>

        <video
          ref={videoRef}
          preload='metadata'
          playsInline
          muted
          onTimeUpdate={(e: any) => setVideoProgress(e.target.currentTime)}
          width={width ? width.toString() : ""}
          height={height ? height.toString() : ""}
        >
          {videoSizePostfix !== null ? (
            <>
              <source
                src={`${src}${videoSizePostfix}.webm`}
                type='video/webm'
              />
              <source
                src={`${src}${videoSizePostfix}.mov`}
                type='video/quicktime'
              />
            </>
          ) : (
            []
          )}
        </video>
        {children}
      </section>
    </>
  );
}
