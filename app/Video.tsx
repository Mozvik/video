"use client";
import { useEffect, useRef, useState } from "react";
import gsap from "gsap";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";

type Props = {
  src: string;
  steps: number[];
  onStepStarted?: Function;
  onVideoEnded?: Function;
  once?: boolean;
  delayAfterEnd?: number;
  width?: string | number;
  height?: string | number;
  children?: React.ReactNode;
};

export default function Video({
  src,
  steps,
  onStepStarted,
  onVideoEnded,
  once,
  delayAfterEnd,
  width,
  height,
  children,
}: Props) {
  gsap.registerPlugin(ScrollTrigger);

  const videoRef = useRef<HTMLVideoElement>(null);
  const [duration, setDuration] = useState(0);
  const [currentStep, setCurrentStep] = useState(0);
  const [videoProgress, setVideoProgress] = useState(0);
  const [videoSizePostfix, setVideoSizePostfix] = useState<null | "" | "_2x">(
    null
  );

  useEffect(() => {
    if (videoRef.current && duration) {
      if (
        videoRef.current.currentTime >= steps[currentStep] &&
        !videoRef.current.paused &&
        !videoRef.current.ended
      ) {
        onStepStarted ? onStepStarted(currentStep) : null;
        setCurrentStep(currentStep + 1);
      }

      if (videoRef.current.ended) {
        onVideoEnded ? onVideoEnded(true) : null;
        if (!once) {
          setTimeout(
            () => {
              setCurrentStep(0);
              videoRef.current?.play();
            },
            delayAfterEnd ? delayAfterEnd : 0
          );
        }
      }
    }
  }, [
    duration,
    currentStep,
    steps,
    videoProgress,
    onStepStarted,
    onVideoEnded,
    delayAfterEnd,
    once,
  ]);

  useEffect(() => {
    if (steps.length < 2) {
      return;
    }

    const matches =
      window.matchMedia("(max-resolution: 1x)").matches ||
      window.matchMedia("(-webkit-max-device-pixel-ratio: 1");
    setVideoSizePostfix(matches ? "_2x" : "");

    if (videoRef.current) {
      videoRef.current.load();

      videoRef.current.addEventListener("loadedmetadata", () => {
        if (videoRef.current?.duration) {
          setDuration(videoRef.current.duration);
          videoRef.current.play();
        }
      });
    }
  }, []);

  return (
    <>
      <div style={{ background: "gainsboro" }}>
        <h2>{duration ? `Video duration: ${duration}` : "video loading..."}</h2>
        <h2>{`steps: ${steps}`}</h2>
        <h2>{`to ${steps[currentStep]} sec`}</h2>

        <video
          ref={videoRef}
          preload='metadata'
          playsInline
          muted
          onTimeUpdate={(e: any) => setVideoProgress(e.target.currentTime)}
          width={width ? width.toString() : ""}
          height={height ? height.toString() : ""}
        >
          {videoSizePostfix !== null ? (
            <>
              <source
                src={`${src}${videoSizePostfix}.webm`}
                type='video/webm'
              />
              <source
                src={`${src}${videoSizePostfix}.mov`}
                type='video/quicktime'
              />
            </>
          ) : (
            []
          )}
        </video>
        {children}
      </div>
    </>
  );
}
