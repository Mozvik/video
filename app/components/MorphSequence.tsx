"use client";
import { useEffect, useRef, useState } from "react";
import classes from "./MorphSequence.module.css";
import gsap from "gsap";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";
gsap.registerPlugin(ScrollTrigger);
type Props = {
  pinnedElementId: string;
  frameCount: number;
  fileExtension?: string;
  width?: number;
  height?: number;
  children?: React.ReactNode;
};

export default function MorphSequence({
  pinnedElementId,
  frameCount,
  fileExtension,
  width,
  height,
  children,
}: Props) {
  const canvas = useRef<HTMLCanvasElement>(null);
  const refContainer = useRef<HTMLDivElement>(null);

  useEffect(() => {
    let images: any[] = [];
    let tower = {
      frame: 0,
    };
    let context = canvas.current?.getContext("2d");

    gsap.matchMedia().add("(min-width: 1024px)", () => {
      if (canvas.current && refContainer.current) {
        canvas.current.width = 600;
        canvas.current.height = 600;
        let url = "/videos/sequences/morph/2x/image-";
        let ext = fileExtension ? fileExtension : "jpg";

        const currentFrame = (index: number) =>
          `${url}${(index + 1).toString().padStart(3, "0")}.${ext}`;
        for (let i = 0; i < frameCount; i++) {
          let img = new Image();
          img.src = currentFrame(i);
          images.push(img);
        }
        const headings = gsap.utils.toArray(
          "h2",
          refContainer.current
        ) as HTMLHeadingElement[];
        const text = gsap.utils.toArray(
          "p",
          refContainer.current
        ) as HTMLParagraphElement[];

        gsap.set(headings, { opacity: 0, color: "red" });
        gsap.set(headings[0], { opacity: 1 });
        gsap.set(text, { opacity: 0, color: "navyblue" });
        gsap.set(text[0], { opacity: 1 });

        gsap
          .timeline({
            onUpdate: render,
            scrollTrigger: {
              trigger: `#${pinnedElementId}`,
              pin: true,
              pinSpacing: true,
              scrub: 0.55,
              end: "+=400%",
              markers: true,
            },
          })
          .to(
            tower,
            {
              frame: frameCount - 1,
              snap: "frame",
              ease: "none",
              duration: 5.5,
            },
            0
          )
          .to(headings[0], { duration: 0.25, opacity: 1 }, 0)
          .to(text[0], { duration: 0.25, opacity: 1 }, 0)
          .to(headings[0], { duration: 0.25, opacity: 0, y: "-=70" }, 0.2)
          .to(text[0], { duration: 0.3, opacity: 0, y: "-=50" }, 0.2)
          .to(headings[1], { duration: 0.25, opacity: 1, y: "-=50" }, 0.2)
          .to(text[1], { duration: 0.3, opacity: 1, y: "-=30" }, 0.2)
          .to(headings[1], { duration: 0.25, opacity: 0, y: "-=70" }, 0.6)
          .to(text[1], { duration: 0.3, opacity: 0, y: "-=50" }, 0.6)
          .to(headings[2], { duration: 0.25, opacity: 1, y: "-=50" }, 0.6)
          .to(text[2], { duration: 0.3, opacity: 1, y: "-=30" }, 0.6)
          .to(headings[2], { duration: 0.25, opacity: 0, y: "-=70" }, 0.9)
          .to(text[2], { duration: 0.3, opacity: 0, y: "-=50" }, 0.9)
          .to(headings[3], { duration: 0.25, opacity: 1, y: "-=50" }, 0.9)
          .to(text[3], { duration: 0.3, opacity: 1, y: "-=30" }, 0.9)
          .to(headings[3], { duration: 1.0, opacity: 1 });
        images[0].onload = render;
      }
      function render() {
        context?.clearRect(
          0,
          0,
          canvas.current?.width!,
          canvas.current?.height!
        );
        context?.drawImage(images[tower.frame], 0, 0, 600, 600);
      }
    });
    gsap.matchMedia().add("(max-width: 1023px)", () => {
      if (canvas.current && refContainer.current) {
        canvas.current.width = window.innerWidth;
        canvas.current.height = 600;
        let url = "/videos/sequences/morph/2x/image-";
        let ext = fileExtension ? fileExtension : "jpg";

        const currentFrame = (index: number) =>
          `${url}${(index + 1).toString().padStart(3, "0")}.${ext}`;
        for (let i = 0; i < frameCount; i++) {
          let img = new Image();
          img.src = currentFrame(i);
          images.push(img);
        }
        const headings = gsap.utils.toArray(
          "h2",
          refContainer.current
        ) as HTMLHeadingElement[];
        const text = gsap.utils.toArray(
          "p",
          refContainer.current
        ) as HTMLParagraphElement[];

        gsap.set(headings, { opacity: 0, color: "red" });
        gsap.set(headings[0], { opacity: 1 });
        gsap.set(text, { opacity: 0, color: "navyblue" });
        gsap.set(text[0], { opacity: 1 });

        gsap
          .timeline({
            onUpdate: render,
            scrollTrigger: {
              trigger: `#${pinnedElementId}`,
              pin: true,
              pinSpacing: true,
              scrub: 0.55,
              end: "+=400%",
              markers: true,
            },
          })
          .to(
            tower,
            {
              frame: frameCount - 1,
              snap: "frame",
              ease: "none",
              duration: 5.5,
            },
            0
          )
          .to(headings[0], { duration: 0.25, opacity: 1 }, 0)
          .to(text[0], { duration: 0.25, opacity: 1 }, 0)
          .to(headings[0], { duration: 0.25, opacity: 0, y: "-=70" }, 0.2)
          .to(text[0], { duration: 0.3, opacity: 0, y: "-=50" }, 0.2)
          .to(headings[1], { duration: 0.25, opacity: 1, y: "-=50" }, 0.2)
          .to(text[1], { duration: 0.3, opacity: 1, y: "-=30" }, 0.2)
          .to(headings[1], { duration: 0.25, opacity: 0, y: "-=70" }, 0.6)
          .to(text[1], { duration: 0.3, opacity: 0, y: "-=50" }, 0.6)
          .to(headings[2], { duration: 0.25, opacity: 1, y: "-=50" }, 0.6)
          .to(text[2], { duration: 0.3, opacity: 1, y: "-=30" }, 0.6)
          .to(headings[2], { duration: 0.25, opacity: 0, y: "-=70" }, 0.9)
          .to(text[2], { duration: 0.3, opacity: 0, y: "-=50" }, 0.9)
          .to(headings[3], { duration: 0.25, opacity: 1, y: "-=50" }, 0.9)
          .to(text[3], { duration: 0.3, opacity: 1, y: "-=30" }, 0.9)
          .to(headings[3], { duration: 1.0, opacity: 1 });
        images[0].onload = render;
      }
      function render() {
        context?.clearRect(
          0,
          0,
          canvas.current?.width!,
          canvas.current?.height!
        );
        context?.drawImage(
          images[tower.frame],
          (window.innerWidth - 600) / 2,
          0,
          600,
          600
        );
      }
    });
  }, []);

  function initCanvas(canvas: HTMLCanvasElement) {
    // let context = canvas.getContext("2d");
    // canvas.width = 500;
    // canvas.height = 500;
    // const currentFrame = (index: number) =>
    //   `/videos/sequences/morph/2x/image-${(index + 1)
    //     .toString()
    //     .padStart(3, "0")}.jpg`;
    // let images: any[] = [];
    // let tower = {
    //   frame: 0,
    // };
    // for (let i = 0; i < frameCount; i++) {
    //   let img = new Image();
    //   img.src = currentFrame(i);
    //   images.push(img);
    // }
    // const headings = gsap.utils.toArray(
    //   "h2",
    //   refContainer.current
    // ) as HTMLHeadingElement[];
    // const text = gsap.utils.toArray(
    //   "p",
    //   refContainer.current
    // ) as HTMLParagraphElement[];
    // gsap.set(headings, { opacity: 0, color: "red" });
    // gsap.set(headings[0], { opacity: 1 });
    // gsap.set(text, { opacity: 0, color: "navyblue" });
    // gsap.set(text[0], { opacity: 1 });
    // gsap
    //   .timeline({
    //     onUpdate: render,
    //     scrollTrigger: {
    //       trigger: `#${pinnedElementId}`,
    //       pin: true,
    //       pinSpacing: true,
    //       scrub: 0.95,
    //       end: "+=400%",
    //       markers: true,
    //     },
    //   })
    //   .to(
    //     tower,
    //     {
    //       frame: frameCount - 1,
    //       snap: "frame",
    //       ease: "none",
    //       duration: 1,
    //     },
    //     0
    //   )
    //   .to(headings[0], { duration: 0.25, opacity: 1 }, 0)
    //   .to(text[0], { duration: 0.25, opacity: 1 }, 0)
    //   .to(headings[0], { duration: 0.25, opacity: 0, y: "-=70" }, 0.2)
    //   .to(text[0], { duration: 0.3, opacity: 0, y: "-=50" }, 0.2)
    //   .to(headings[1], { duration: 0.25, opacity: 1, y: "-=50" }, 0.2)
    //   .to(text[1], { duration: 0.3, opacity: 1, y: "-=30" }, 0.2)
    //   .to(headings[1], { duration: 0.25, opacity: 0, y: "-=70" }, 0.6)
    //   .to(text[1], { duration: 0.3, opacity: 0, y: "-=50" }, 0.6)
    //   .to(headings[2], { duration: 0.25, opacity: 1, y: "-=50" }, 0.6)
    //   .to(text[2], { duration: 0.3, opacity: 1, y: "-=30" }, 0.6)
    //   .to(headings[2], { duration: 0.25, opacity: 0, y: "-=70" }, 0.9)
    //   .to(text[2], { duration: 0.3, opacity: 0, y: "-=50" }, 0.9)
    //   .to(headings[3], { duration: 0.25, opacity: 1, y: "-=50" }, 0.9)
    //   .to(text[3], { duration: 0.3, opacity: 1, y: "-=30" }, 0.9)
    //   .to(headings[3], { duration: 1.0, opacity: 1 });
    // images[0].onload = render;
    // function render() {
    //   context?.clearRect(0, 0, canvas.width, canvas.height);
    //   context?.drawImage(images[tower.frame], 0, 0, 500, 500);
    // }
  }

  return (
    <>
      <div className={`${classes.wrapper}`}>
        {children}
        <canvas style={{ position: "fixed", zIndex: -1 }} ref={canvas}></canvas>
        <div ref={refContainer} className={`${classes.ul}`}>
          <div>
            <h2>0 Lorem ipsum</h2>
            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit.
              Perspiciatis fugit placeat voluptatum.
            </p>
          </div>
          <div>
            <h2>1 dolor sit</h2>
            <p>
              Lorem ipsum, dolor sit amet consectetur adipisicing elit. Illum
              aut laboriosam nobis a dolor modi esse voluptatum nemo
              exercitationem maxime.
            </p>
          </div>
          <div>
            <h2>2 amet consectetur</h2>
            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Sapiente,
              minima libero!
            </p>
          </div>
          <div>
            <h2>3 adipisicing elit</h2>
            <p>
              Lorem ipsum dolor sit, amet consectetur adipisicing elit. Cumque
              deserunt delectus praesentium quidem!
            </p>
          </div>
        </div>
      </div>
    </>
  );
}
