"use client";
import { useEffect, useRef, useState } from "react";
import classes from "./TowerSequence.module.css";
import gsap from "gsap";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";
gsap.registerPlugin(ScrollTrigger);
type Props = {
  pinnedElementId: string;
  frameCount: number;
  fileExtension?: string;
  width?: number;
  height?: number;
  children?: React.ReactNode;
};

export default function TowerSequence({
  pinnedElementId,
  frameCount,
  fileExtension,
  width,
  height,
  children,
}: Props) {
  const canvas = useRef<HTMLCanvasElement>(null);
  const refUl = useRef<HTMLUListElement>(null);

  useEffect(() => {
    let images: any[] = [];
    let tower = {
      frame: 0,
    };
    let context = canvas.current?.getContext("2d");
    if (canvas.current) {
      // canvas.current.width = width ? width : 600;
      // canvas.current.height = height ? height : 600;
    }

    gsap.matchMedia().add("(min-width: 1024px)", () => {
      if (canvas.current && refUl.current) {
        canvas.current.width = 600;
        canvas.current.height = 600;
        let url = "/videos/sequences/tower/2x/image-";
        let ext = fileExtension ? fileExtension : "jpg";

        const currentFrame = (index: number) =>
          `${url}${(index + 1).toString().padStart(3, "0")}.${ext}`;

        for (let i = 0; i < frameCount; i++) {
          let img = new Image();
          img.src = currentFrame(i);
          images.push(img);
        }
        const list = gsap.utils.toArray("li", refUl.current) as HTMLLIElement[];
        gsap.set(list[5], { x: 240, y: 120, opacity: 0 });
        gsap.set(list[3], { x: 240, y: 50, opacity: 0 });
        gsap.set(list[1], { x: 240, y: -20, opacity: 0 });

        gsap.set(list[6], { x: -240, y: 140, opacity: 0 });
        gsap.set(list[4], { x: -240, y: 80, opacity: 0 });
        gsap.set(list[2], { x: -240, y: 10, opacity: 0 });
        gsap.set(list[0], { x: -240, y: -70, opacity: 0 });

        gsap
          .timeline({
            onUpdate: render,
            scrollTrigger: {
              trigger: `#${pinnedElementId}`,
              pin: true,
              pinSpacing: true,
              scrub: 0.55,
              end: "+=500%",
              markers: true,
            },
          })
          .to(
            tower,
            {
              frame: frameCount - 1,
              snap: "frame",
              ease: "none",
              duration: 1.5,
            },
            0
          )
          .to(list[6], { x: -200, duration: 0.5, opacity: 1 }, 0)
          .to(list[5], { x: 200, duration: 0.5, opacity: 1 }, 0.2)
          .to(list[4], { x: -200, duration: 0.5, opacity: 1 }, 0.3)
          .to(list[3], { x: 200, duration: 0.5, opacity: 1 }, 0.46)
          .to(list[2], { x: -200, duration: 0.5, opacity: 1 }, 0.55)
          .to(list[1], { x: 200, duration: 0.5, opacity: 1 }, 0.62)
          .to(list[0], { x: -200, duration: 0.5, opacity: 1 }, 0.7)

          .to(canvas.current, { x: "-50vw", delay: 0.5, duration: 0.5 }, 0.87)
          .to(
            list,
            {
              x: "-35vw",
              y: 45,
              marginBottom: 20,
              delay: 0.5,
              duration: 0.5,
            },
            0.9
          )
          .to(list, {
            opacity: 1,
            duration: 0.5,
          });
        images[0].onload = render;
      }
      function render() {
        context?.clearRect(
          0,
          0,
          canvas.current?.width!,
          canvas.current?.height!
        );
        context?.drawImage(
          images[tower.frame],
          0,
          0,
          width ? width : 600,
          height ? height : 600
        );
      }
    });

    gsap.matchMedia().add("(max-width: 1023px)", () => {
      if (canvas.current && refUl.current) {
        canvas.current.width = window.innerWidth;
        canvas.current.height = 600;
        let url = "/videos/sequences/tower/1x/image-";
        let ext = fileExtension ? fileExtension : "jpg";

        const currentFrame = (index: number) =>
          `${url}${(index + 1).toString().padStart(3, "0")}.${ext}`;

        for (let i = 0; i < frameCount; i++) {
          let img = new Image();
          img.src = currentFrame(i);
          images.push(img);
        }
        const list = gsap.utils.toArray("li", refUl.current) as HTMLLIElement[];
        gsap.set(list[5], { x: 240, y: 120, opacity: 0 });
        gsap.set(list[3], { x: 240, y: 50, opacity: 0 });
        gsap.set(list[1], { x: 240, y: -20, opacity: 0 });

        gsap.set(list[6], { x: -240, y: 140, opacity: 0 });
        gsap.set(list[4], { x: -240, y: 80, opacity: 0 });
        gsap.set(list[2], { x: -240, y: 10, opacity: 0 });
        gsap.set(list[0], { x: -240, y: -70, opacity: 0 });

        gsap
          .timeline({
            onUpdate: render,
            scrollTrigger: {
              trigger: `#${pinnedElementId}`,
              pin: true,
              pinSpacing: true,
              scrub: 0.55,
              end: "+=500%",
              markers: true,
            },
          })
          .to(
            tower,
            {
              frame: frameCount - 1,
              snap: "frame",
              ease: "none",
              duration: 1.5,
            },
            0
          )
          .to(list[6], { x: -200, duration: 0.5, opacity: 1 }, 0)
          .to(list[5], { x: 200, duration: 0.5, opacity: 1 }, 0.2)
          .to(list[4], { x: -200, duration: 0.5, opacity: 1 }, 0.3)
          .to(list[3], { x: 200, duration: 0.5, opacity: 1 }, 0.46)
          .to(list[2], { x: -200, duration: 0.5, opacity: 1 }, 0.55)
          .to(list[1], { x: 200, duration: 0.5, opacity: 1 }, 0.62)
          .to(list[0], { x: -200, duration: 0.5, opacity: 1 }, 0.7)

          .to(canvas.current, { x: "-50vw", delay: 0.5, duration: 0.5 }, 0.87)
          .to(
            list,
            {
              x: "-35vw",
              y: 45,
              marginBottom: 20,
              delay: 0.5,
              duration: 0.5,
            },
            0.9
          )
          .to(list, {
            opacity: 1,
            duration: 0.5,
          });
        images[0].onload = render;
      }
      function render() {
        context?.clearRect(
          0,
          0,
          canvas.current?.width!,
          canvas.current?.height!
        );
        context?.drawImage(
          images[tower.frame],
          (window.innerWidth - 600) / 2,
          0,
          600,
          600
        );
      }
    });
  }, []);

  function initCanvas(canvas: HTMLCanvasElement) {}

  return (
    <>
      <div className={`${classes.wrapper}`}>
        {children}
        <canvas style={{ position: "fixed", zIndex: -1 }} ref={canvas}></canvas>
        <ul ref={refUl} className={`${classes.ul}`}>
          <li>0 Lorem ipsum</li>
          <li>1 dolor sit</li>
          <li>2 amet consectetur</li>
          <li>3 adipisicing elit</li>
          <li>4 Nihil molestiae</li>
          <li>5 sint explicabo</li>
          <li>6 eius magnam</li>
        </ul>
      </div>
    </>
  );
}
