"use client";
import {
  SyntheticEvent,
  useCallback,
  useEffect,
  useRef,
  useState,
} from "react";
import "../styles/globals.css";
import gsap from "gsap";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";
import TowerSequence from "./components/TowerSequence";
import MorphSequence from "./components/MorphSequence";

gsap.registerPlugin(ScrollTrigger);

export default function Page() {
  const towerSteps = [1.55, 2.88, 4.2, 5.6, 7.05, 8.45, 10];

  // const onEndUN = useCallback((status: any) => {
  //   console.log("-UN video end ", status);
  // }, []);

  return (
    <>
      <main>
        <section>
          <img src='/videos/image-033.webP' alt='' />
          <h1>Scroll down</h1>
          <h2>{process.env.SERVER}</h2>
          <h2> image: {process.env.IMAGE}</h2>
          {/* <video src="/videos/tower_2x.webm" controls></video> */}
        </section>
        <section id='pinnedSnap' style={{ background: "white" }}>
          <br />
          <br />
          <h1>152 frames, jpg 3.2mb</h1>
          <TowerSequence
            pinnedElementId={"pinnedSnap"}
            frameCount={515}
            fileExtension={"webp"}
          />
          <h1>Scroll down</h1>
          <br />
          <br />
        </section>
        <section id='pinnedMorphSnap' style={{ background: "gainsboro" }}>
          <br />
          <br />
          <h1>152 frames, jpg 7.5mb</h1>
          {/* <MorphSequence
            pinnedElementId={"pinnedMorphSnap"}
            frameCount={601}
            fileExtension={"webp"}
          /> */}
          <h1>Scroll down</h1>
          <br />
          <br />
        </section>
        <section></section>
        <section style={{ background: "green" }}></section>
      </main>
    </>
  );
}
