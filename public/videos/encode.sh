!/usr/bin/env bash

if [[ -z "$1" || ! -d "$1" ]]; then
  echo "The first parameter should be an existing directory which contains the original files."
  exit 1
fi

echo "Encoding loading animation..."
echo "Apple (1/4) "
ffmpeg -y -sseof -7 -c:v libvpx-vp9 -i $1/GB-SOLUTIONS_intuitech_website-scroll-animation_MORPH_4k@60fps+alpha.webm -filter:v "scale=178:100,crop=100:100:39:0,setpts=0.33*PTS" -c:v hevc_videotoolbox -allow_sw 1 -alpha_quality 1 -pix_fmt bgra -vtag hvc1 -b:v 500k loading.mov 2> /dev/null
echo "Apple 2x (2/4)"
ffmpeg -y -sseof -7 -c:v libvpx-vp9 -i $1/GB-SOLUTIONS_intuitech_website-scroll-animation_MORPH_4k@60fps+alpha.webm -filter:v "scale=356:200,crop=200:200:78:0,setpts=0.33*PTS" -c:v hevc_videotoolbox -allow_sw 1 -alpha_quality 1 -pix_fmt bgra -vtag hvc1 -b:v 500k loading_2x.mov 2> /dev/null
echo "Non-Apple (3/4)"
ffmpeg -y -sseof -7 -c:v libvpx-vp9 -i $1/GB-SOLUTIONS_intuitech_website-scroll-animation_MORPH_4k@60fps+alpha.webm -filter:v "scale=178:100,crop=100:100:39:0,setpts=0.33*PTS" -c:v libvpx-vp9 -pix_fmt yuva420p -crf 52 loading.webm 2> /dev/null
echo "Non-Apple 2x (4/4)"
ffmpeg -y -sseof -7 -c:v libvpx-vp9 -i $1/GB-SOLUTIONS_intuitech_website-scroll-animation_MORPH_4k@60fps+alpha.webm -filter:v "scale=356:200,crop=200:200:78:0,setpts=0.33*PTS" -c:v libvpx-vp9 -pix_fmt yuva420p -crf 52 loading_2x.webm 2> /dev/null


echo "Encoding tower animation..."
echo "Apple (1/4)"
ffmpeg -y -c:v libvpx-vp9 -i $1/GB-SOLUTIONS_intuitech_website-scroll-animation_TOWER_4k@60fps+alpha.webm -filter:v crop=2156:2156,scale=-1:450 -c:v hevc_videotoolbox -allow_sw 1 -alpha_quality 1 -allow_sw 1 -pix_fmt bgra -vtag hvc1 -b:v 500k tower.mov 2> /dev/null
echo "Apple 2x (2/4)"
ffmpeg -y -c:v libvpx-vp9 -i $1/GB-SOLUTIONS_intuitech_website-scroll-animation_TOWER_4k@60fps+alpha.webm -filter:v crop=2156:2156,scale=-1:900 -c:v hevc_videotoolbox -allow_sw 1 -alpha_quality 1 -pix_fmt bgra -vtag hvc1 -b:v 800k tower_2x.mov 2> /dev/null
echo "Non-Apple (3/4)"
ffmpeg -y -c:v libvpx-vp9 -i $1/GB-SOLUTIONS_intuitech_website-scroll-animation_TOWER_4k@60fps+alpha.webm -filter:v crop=2156:2156,scale=-1:450 -c:v libvpx-vp9 -pix_fmt yuva420p -crf 40 tower.webm 2> /dev/null
echo "Non-Apple 2x (4/4)"
ffmpeg -y -c:v libvpx-vp9 -i $1/GB-SOLUTIONS_intuitech_website-scroll-animation_TOWER_4k@60fps+alpha.webm -filter:v crop=2156:2156,scale=-1:900 -c:v libvpx-vp9 -pix_fmt yuva420p -crf 40 tower_2x.webm 2> /dev/null

echo "Encoding morph animation..."
echo "Apple (1/4)"
ffmpeg -y -c:v libvpx-vp9 -i $1/GB-SOLUTIONS_intuitech_website-scroll-animation_MORPH_4k@60fps+alpha.webm -filter:v crop=2156:2156,scale=-1:450 -c:v hevc_videotoolbox -allow_sw 1 -alpha_quality 0.75 -pix_fmt bgra -b:v 500k -vtag hvc1 morph.mov 2> /dev/null
echo "Apple 2x (2/4)"
ffmpeg -y -c:v libvpx-vp9 -i $1/GB-SOLUTIONS_intuitech_website-scroll-animation_MORPH_4k@60fps+alpha.webm -filter:v crop=2156:2156,scale=-1:900 -c:v hevc_videotoolbox -allow_sw 1 -alpha_quality 1 -pix_fmt bgra -b:v 800k -vtag hvc1 morph_2x.mov 2> /dev/null
echo "Non-Apple (3/4)"
ffmpeg -y -c:v libvpx-vp9 -i $1/GB-SOLUTIONS_intuitech_website-scroll-animation_MORPH_4k@60fps+alpha.webm -filter:v crop=2156:2156,scale=-1:450 -c:v libvpx-vp9 -pix_fmt yuva420p -crf 40 morph.webm 2> /dev/null
echo "Non-Apple 2x (4/4)"
ffmpeg -y -c:v libvpx-vp9 -i $1/GB-SOLUTIONS_intuitech_website-scroll-animation_MORPH_4k@60fps+alpha.webm -filter:v crop=2156:2156,scale=-1:900 -c:v libvpx-vp9 -pix_fmt yuva420p -crf 40 morph_2x.webm 2> /dev/null

echo "Encoding u-shape animation"
echo "Apple (1/4)"
ffmpeg -y -c:v libvpx-vp9 -i $1/GB-SOLUTIONS_intuitech_website-scroll-animation_UN-SHAPE_4k@60fps+alpha.webm -filter:v crop=2156:2156,scale=-1:450 -c:v hevc_videotoolbox -allow_sw 1 -alpha_quality 1 -pix_fmt bgra -b:v 800k -vtag hvc1 u-shape.mov 2> /dev/null
echo "Apple 2x (2/4)"
ffmpeg -y -c:v libvpx-vp9 -i $1/GB-SOLUTIONS_intuitech_website-scroll-animation_UN-SHAPE_4k@60fps+alpha.webm -filter:v crop=2156:2156,scale=-1:900 -c:v hevc_videotoolbox -allow_sw 1 -alpha_quality 1 -pix_fmt bgra -b:v 1000k -vtag hvc1 u-shape_2x.mov 2> /dev/null
echo "Non-Apple (3/4)"
ffmpeg -y -c:v libvpx-vp9 -i $1/GB-SOLUTIONS_intuitech_website-scroll-animation_UN-SHAPE_4k@60fps+alpha.webm -filter:v crop=2156:2156,scale=-1:562 -c:v libvpx-vp9 -pix_fmt yuva420p -crf 40 u-shape.webm 2> /dev/null
echo "Non-Apple 2x (4/4)"
ffmpeg -y -c:v libvpx-vp9 -i $1/GB-SOLUTIONS_intuitech_website-scroll-animation_UN-SHAPE_4k@60fps+alpha.webm -filter:v crop=2156:2156,scale=-1:1124 -c:v libvpx-vp9 -pix_fmt yuva420p -crf 40 u-shape_2x.webm 2> /dev/null



ffmpeg -c:v libvpx-vp9 -i GB-SOLUTIONS_intuitech_website-scroll-animation_TOWER_4k@60fps+alpha.webm -filter:v crop=2156:2156,scale=-1:600 -r 15 -qscale:v 2 sequences/tower/2x/image-%03d.png
magick mogrify -format jpg -background '#f3f4f6' -flatten -path sequences/tower/2x/jpg sequences/tower/2x/*.png

magick mogrify -format webP -quality 80 -define webp:lossless=true -path sequences/tower/2x/webp sequences/tower/2x/*.png
magick mogrify -format webP -quality 90 -path sequences/tower/2x/webp-lossy-80 sequences/tower/2x/*.png

ffmpeg -c:v libvpx-vp9 -i GB-SOLUTIONS_intuitech_website-scroll-animation_TOWER_4k@60fps+alpha.webm -filter:v crop=2156:2156,scale=-1:400 -r 15 -qscale:v 2 sequences/tower/1x/image-%03d.png
magick mogrify -format jpg -background '#f3f4f6' -flatten -path sequences/tower/1x/jpg sequences/tower/1x/*.png


ffmpeg -c:v libvpx-vp9 -i GB-SOLUTIONS_intuitech_website-scroll-animation_MORPH_4k@60fps+alpha.webm -filter:v crop=2156:2156,scale=-1:600 -r 15 -qscale:v 2 sequences/morph/2x/image-%03d.png
magick mogrify -format jpg -background white -quality 100 -flatten -path sequences/morph/2x/jpg sequences/morph/2x/*.png
ffmpeg -c:v libvpx-vp9 -i GB-SOLUTIONS_intuitech_website-scroll-animation_MORPH_4k@60fps+alpha.webm -filter:v crop=2156:2156,scale=-1:400 -r 15 -qscale:v 2 sequences/morph/1x/image-%03d.png
magick mogrify -format jpg -background white -quality 100 -flatten -path sequences/morph/1x/jpg sequences/morph/1x/*.png




